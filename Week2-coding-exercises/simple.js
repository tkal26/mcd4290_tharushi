//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
//question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    let array=[54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, 
-51, -17, -25];


    let positiveOdd =[];
    let negativeEven =[];

    for (var i =0; i<array.length;i++ ){
        if ((array[i] % 2 ==1) && (array[i]>0)){
            positiveOdd.push(array[i]);
        }
    else if ((array[i] % 2==0) && (array[i]<0)){
        negativeEven.push(array[i]);       
        }
    }

    output += "Positive Odd: ";
    for (var i =0; i<positiveOdd.length;i++ ){
        output += positiveOdd[i];
        if (positiveOdd.length!= i+1){
            output += ", ";
        }
    }

    output += "\n";
    output += "Negative Even : ";

    for (var i =0; i<negativeEven.length;i++ ){
        output += negativeEven[i];
        if (negativeEven.length!= i+1){
            output += ", ";
        }
    }
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    //part 1
    let x=[0, 0, 0, 0, 0, 0];
for(var i=0;i<60001;i++)
  {
    var roll= Math.floor((Math.random() * 6) + 1);
    
    if(roll==1)
      {
        x[0]+=1;
      }
      else if(roll==2)
      {
        x[1]+=1;
      }
      else if(roll==3)
      {
        x[2]+=1;
      }
      else if(roll==4)
      {
        x[3]+=1;
      }
      else if(roll==5)
      {
        x[4]+=1;
      }
        else if(roll==6)
      {
        x[5]+=1;
      }     
      
  }
    output=("Frequency of Die Rolls:"+"\n"+"1: "+x[0]+"\n"+"2: "+x[1]+"\n"+"3: "+x[2]+"\n"+"4: "+x[3]+"\n"+"5: "+x[4]+"\n"+"6: "+x[5])
    
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //part 2
    let y=[0, 0, 0, 0, 0, 0, 0];
for(var i=0;i<60001;i++)
  {
    var roll= Math.floor((Math.random() * 6) + 1);
    
    if(roll==1)
      {
        y[1]+=1;
      }
      else if(roll==2)
      {
        y[2]+=1;
      }
      else if(roll==3)
      {
        y[3]+=1;
      }
      else if(roll==4)
      {
        y[4]+=1;
      }
      else if(roll==5)
      {
        y[5]+=1;
      }
        else if(roll==6)
      {
        y[6]+=1;
      }     
      
  }

output=("Frequency of Die Rolls:"+"\n"+"1: "+x[1]+"\n"+"2: "+x[2]+"\n"+"3: "+x[3]+"\n"+"4: "+x[4]+"\n"+"5: "+x[5]+"\n"+"6: "+x[6])


    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    let person = {
      name: "Jane",
    income: 127050
}
var tax

if(person.income<=18200)
  {
    tax='0';
  }
else if(person.income>18200 && person.income<=37000)
  {
    tax=(person.income-18200)*0.19;
  }
else if(person.income>37000 && person.income<=90000)
  {
    tax=(person.income-37000)*0.325+3572;
  }
else if(person.income>90000 && person.income<=180000)
  {
    tax=(person.income-90000)*0.37+20797;
  }
else if(person.income>180000)
  {
    tax=(person.income-180000)*0.45+54097;
  }

output=(person.name+"'s income is: $"+ person.income+" , and her tax owed is: $"+ tax.toFixed(2));
    
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}